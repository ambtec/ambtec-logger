"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

class Log {
  constructor() {
    this.logDefault = {
      breadcrumbs: [],
      message: '',
      properties: {}
    };
    this.logStyles = void 0;

    const {
      EventOn
    } = require('promiseevents');

    this.logStyles = {
      reset: '\x1b[0m',
      bright: '\x1b[1m',
      dim: '\x1b[2m',
      underscore: '\x1b[4m',
      blink: '\x1b[5m',
      reverse: '\x1b[7m',
      hidden: '\x1b[8m',
      // Foreground (text) colors
      fg: {
        black: '\x1b[30m',
        red: '\x1b[31m',
        green: '\x1b[32m',
        yellow: '\x1b[33m',
        blue: '\x1b[34m',
        magenta: '\x1b[35m',
        cyan: '\x1b[36m',
        white: '\x1b[37m',
        crimson: '\x1b[38m'
      },
      // Background colors
      bg: {
        black: '\x1b[40m',
        red: '\x1b[41m',
        green: '\x1b[42m',
        yellow: '\x1b[43m',
        blue: '\x1b[44m',
        magenta: '\x1b[45m',
        cyan: '\x1b[46m',
        white: '\x1b[47m',
        crimson: '\x1b[48m'
      }
    };
    EventOn('Log.trace', this.trace.bind(this));
    EventOn('Log.debug', this.debug.bind(this));
    EventOn('Log.info', this.info.bind(this));
    EventOn('Log.success', this.success.bind(this));
    EventOn('Log.notice', this.notice.bind(this));
    EventOn('Log.warning', this.warning.bind(this));
    EventOn('Log.error', this.error.bind(this));
    EventOn('Log.crit', this.crit.bind(this));
    EventOn('Log.alert', this.alert.bind(this));
    EventOn('Log.emerg', this.emerg.bind(this));
  }
  /**
   * Get default configuration build
   *
   * @param {*} config
   * @returns object
   */


  normalize(config) {
    const temp = { ...this.logDefault,
      ...config
    };
    return temp;
  }
  /**
   * Get standardized log message with breadcrumbs
   *
   * @param {*} config
   * @returns string
   */


  buildMessage(config) {
    let msg = '';

    if (config.breadcrumbs.length) {
      msg = `[${config.breadcrumbs.join('][')}] `;
    }

    if (config.message) msg += `${config.message} `;
    if (Object.keys(config.properties).length) msg += `${JSON.stringify(config.properties)} `;
    return msg;
  }

  async trace(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['trace'], ...config.breadcrumbs];
    console.trace(`${this.logStyles.fg.magenta}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async debug(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['debug'], ...config.breadcrumbs];
    console.debug(`${this.logStyles.fg.magenta}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async info(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['info'], ...config.breadcrumbs];
    console.log(`${this.logStyles.fg.white}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async success(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['success'], ...config.breadcrumbs];
    console.log(`${this.logStyles.fg.green}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async notice(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['notice'], ...config.breadcrumbs]; // @ts-ignore

    console.notice(`${this.logStyles.fg.white}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async warning(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['warning'], ...config.breadcrumbs];
    console.warn(`${this.logStyles.fg.yellow}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async error(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['error'], ...config.breadcrumbs];
    console.error(`${this.logStyles.fg.red}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async crit(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['crit'], ...config.breadcrumbs]; // @ts-ignore

    console.crit(`${this.logStyles.bg.red}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async alert(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['alert'], ...config.breadcrumbs]; // @ts-ignore

    console.alert(`${this.logStyles.bg.red}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

  async emerg(config) {
    config = this.normalize(config);
    config.breadcrumbs = [...['emerg'], ...config.breadcrumbs]; // @ts-ignore

    console.emerg(`${this.logStyles.bg.red}%s${this.logStyles.reset}`, this.buildMessage(config));
  }

}

var _default = new Log();

exports.default = _default;