# ambtec logger

A ambtec logger wrapper to standardize log outputs for ambtec projects

## Usage

Logger consists of three parts breadcrumbs, message and properties.

### breadcrumbs

An array of string identifiers which helps to track down th event route. Can be everything meaningfull you like like for example module id, session id, user id, request id, ...
### message

The basic message to log

### properties

An object of properties which might come handy and therefor should be logged, too

## Structure

### Logger body

The expected body to log

``
{
  breadcrumbs: [],
  message: '',
  properties: {}
}
``

### Logger endpoints

The supported log levels are `` trace ``, `` debug ``, `` info ``, `` success ``, `` notice ``, `` warning ``, `` error ``, `` crit ``, `` alert ``, `` emerg ``. \
Learn more about it at https://www.loggly.com/ultimate-guide/node-logging-basics/ \
Log level `` success `` is a variant of `` info ``.

### Example

```
const Log = require('ambtec-logger');
Log.debug({
  breadcrumbs: [ 'Example', process.getuid() ],
  message: 'An example log entry',
  properties: {
    hello: 'World'
  }
});
```

will output:

```
[debug][Example][1234567890] An example log entry {"hello":"World"}
```

## License
MIT

